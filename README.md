I purchased one of the TI evaluation boards: https://www.ti.com/tool/LAUNCHXL-F280049C and I wanted to run simple echoback using SCI (UART) using interrupts and TI-RTOS Kernel (SYS/BIOS; read more in user guide: https://www.ti.com/tool/SYSBIOS#tech-docs). 
I decided to because I was bored doing the same simple perihperal configs on bare metal applications and facing the problem of handling events in larger apps. The solution is using some real-time operating system and one of them is TI-RTOS.
Issues start when you create new project for SYS/BIOS in CCS and add e.g. SCI configuration. It initiates chain of problems you have to solve which are not obvious for less experienced users like me. In contrast to bare metal examples from TI Resource Explorer which run out of the box without any problems you have to deal with e.g. memory organisation, espacially if you want to flash your program and use it standalone without debugger (it is obvious that this is a goal of every real application). 
I solved a lot of problems with help on TI e2e forum - special thanks to Whitney Dewey; here is link to the topic:
https://e2e.ti.com/support/microcontrollers/c2000/f/c2000-microcontrollers-forum/988696/launchxl-f280049c-sci-interrupts-on-sys-bios/

System information:
* Simple echoback project which uses SCIA routed to debugger to send echoback to PC terminal
* CCS 10.2.0.00009
* SYS/BIOS 6.83.0.18
* LaunchXL-F280049C bought on TI store (fast shipping!)

Feel free to use the code. MIT license and it would be wonderful if you mention me in app credits, manual etc.

BR,
Dawid.

**Contact: https://gitlab.com/dbalcerzak**
