/*
 * hwi_sci.h
 *
 *  Created on: 28 mar 2021
 *      Author: dawid
 */

#ifndef BIOS_HWI_HEADER_HWI_SCI_H_
#define BIOS_HWI_HEADER_HWI_SCI_H_

#include <xdc/runtime/System.h>
#include <ti/sysbios/BIOS.h>

Void sciaRxIsr(UArg arg);
Void sciaTxIsr(UArg arg);

#endif /* BIOS_HWI_HEADER_HWI_SCI_H_ */
