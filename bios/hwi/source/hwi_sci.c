/*
 * hwi_sci.c
 *
 *  Created on: 28 mar 2021
 *      Author: dawid
 */


#include "../header/hwi_sci.h"
#include "driverlib.h"
#include "device.h"

uint16_t dataSent = 0;
uint16_t readData = 0;

__attribute__((ramfunc))
Void sciaRxIsr(UArg arg){
    uint16_t sciStatusBit = SCI_getRxStatus(SCIA_BASE);
    if(sciStatusBit & SCI_RXSTATUS_ERROR){
        SCI_performSoftwareReset(SCIA_BASE);
    }
    if(SCI_isDataAvailableNonFIFO(SCIA_BASE)){
        readData = SCI_readCharNonBlocking(SCIA_BASE);
        SCI_writeCharNonBlocking(SCIA_BASE, readData);
    }else{
        SCI_performSoftwareReset(SCIA_BASE);
    }

    return;
}

__attribute__((ramfunc))
Void sciaTxIsr(UArg arg){
    //todo:
    //ticks++;
    dataSent++;
    return;
}
