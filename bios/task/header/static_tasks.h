/*
 * static_tasks.c
 *
 *  Created on: 28 mar 2021
 *      Author: dawid
 */

#ifndef BIOS_TASK_HEADER_STATIC_TASKS_C_
#define BIOS_TASK_HEADER_STATIC_TASKS_C_

#include <xdc/runtime/System.h>
#include <ti/sysbios/BIOS.h>

Void systemStartInfo(UArg a0, UArg a1);
Void setPeripherals(UArg a0, UArg a1);

#endif /* BIOS_TASK_HEADER_STATIC_TASKS_C_ */
