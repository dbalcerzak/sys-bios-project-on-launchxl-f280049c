/*
 * static_tasks.c
 *
 *  Created on: 28 mar 2021
 *      Author: dawid
 */


#include <Debug/syscfg/board.h>
#include "../header/static_tasks.h"
#include "driverlib.h"
#include "device.h"

Void setPeripherals(UArg a0, UArg a1){
    // PLL is configured by app.cfg
    SysCtl_enablePeripheral(SYSCTL_PERIPH_CLK_SCIA);
    Device_initGPIO();

    Board_init();
    GPIO_setAnalogMode(23U, GPIO_ANALOG_DISABLED); // sprui33d.pdf -> str. 886; GPAAMSEL flag clearance
}



Void systemStartInfo(UArg a0, UArg a1)
{
    System_printf("System start.\n");
    System_printf("SYS/BIOS on LaunchXL-F280049C!\n");
    System_flush(); // force SysMin output to console
}
