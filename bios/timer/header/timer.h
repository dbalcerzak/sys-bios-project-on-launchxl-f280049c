/*
 * timer.h
 *
 *  Created on: 28 mar 2021
 *      Author: dawid
 */

#ifndef BIOS_TIMER_HEADER_TIMER_H_
#define BIOS_TIMER_HEADER_TIMER_H_

#include <xdc/runtime/System.h>
#include <ti/sysbios/BIOS.h>

Void timer_0_isr();

#endif /* BIOS_TIMER_HEADER_TIMER_H_ */
