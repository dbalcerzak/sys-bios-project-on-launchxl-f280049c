/*
 * timer.c
 *
 *  Created on: 28 mar 2021
 *      Author: dawid
 */


#include "../header/timer.h"
#include "driverlib.h"
#include "device.h"

int ticks = 0;

Void timer_0_isr(){
    ticks++;
    GPIO_togglePin(DEVICE_GPIO_PIN_LED1);
}
