/*
 *  ======== main.c ========
 */

//#include <xdc/std.h>
//
//#include <xdc/runtime/Error.h>
#include <xdc/runtime/System.h>
//
#include <ti/sysbios/BIOS.h>
//
//#include <ti/sysbios/knl/Task.h>
//#include <ti/sysbios/hal/Hwi.h>
//
//#include "driverlib.h"
//#include "device.h"
//#include "board.h"

/*
 *  ======== main ========
 */
Int main()
{

    System_printf("\nEnter main(). Program is starting.\n\n");

    BIOS_start();    /* does not return */
    return(0);
}
